# Documentation static site generator & deployment tool
mkdocs>=1.1.2

# Add your custom theme if not inside a theme_dir
# (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
mkdocs-material>=6.1.5
Pygments>=2.4
markdown>=3.2
pymdown-extensions>=7.0
mkdocs-material-extensions>=1.0
mkdocs-git-revision-date-plugin>=0.3.1
mkdocs-macros-plugin>=0.4.20
