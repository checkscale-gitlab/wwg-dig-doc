# Digitale Infrastrukture 2.0

Diese Anleitung hilft dabei einen Server aufzusetzten, der es einer Schule erlaubt Schulsoftware selbstständig zu verwalten.

## 1. Server vorbereiten

### 1. Wahl eines Anbieters
Das Wim-Wenders-Gymnasium hat mehrere Anforderungen an einen Server-Anbieter:

1. Server-Standort in Deutschland aus datenschutzrechtlichen Gründen.
2. Nach Möglichkeit sollen die Server mit Strom aus erneuerbaren Energien betrieben werden.
3. Komplette einfache Selbstverwaltung der Server.
4. Nach Möglichkeit ein etablierter Anbieter um Ausfallsicherheit zu maximieren.

Das Wim-Wenders-Gymnasium hat sich für [Hetzner](https://www.hetzner.com/) entschieden.
Geplant ist ein Cluster aus mehreren virtuellen Servern.
Die Grundversion reicht nach unserer Erfahrung für die meisten Services für eine Schule mit rund 500 Mitgliedern.
Vorteile die ein Cluster bietet:
    * Skalierbarkeit (einfach erweiterbar)
    * High Availability (Einzelne Services bleiben auch während Updates oder Ausfällen verfügbar)
    * Self healing (Fehler können einfach "zurück gerollt" werden)

Wir haben uns für Cloud-Server entschieden, weil diese unkompliziert automatisiert installiert und verwaltet werden können.

![Eine erste Visualisierung der geplanten Infrastruktur.](img/Server_Infrastructure_2.0_-_Mainframe.jpg)

*Eine erste Visualisierung der geplanten Infrastruktur.*

### 2. Wahl der Server
Hier wird eine High Availability [k3s](https://k3s.io/)-Installation ausgeführt.
Als k3s-Distribution wählen wir hier [Rancher](https://rancher.com/), das eine einfache stabile Variante darstellt.

#### Rancher-Server-Nodes
Drei Virtuelle Server stellen die weiteren Services zur Verfügung und sorgen für die hohe Verfügbarkeit.
Geplant sind 3 *CPX31-Server* für jeweils rund *15€*.

#### Rancher-Agent-Nodes
Auf diesen Nodes werden die einzelnen Services verteilt.
Geplant sind vier *CPX41-Server* für jeweils rund *30€*.
